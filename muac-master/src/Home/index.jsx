import { Box, Fab, Stack } from '@mui/material';
import { useNavigate } from 'react-router';

const Home = () => {
  const buttonsSize = {height: '20vh', width: '20vh'};
  const navigate = useNavigate();

	return (
		<Stack spacing={5}>
      <Fab onClick={() => navigate('pregnant')} sx={{backgroundColor: '#f59fac', ...buttonsSize}}>
        <Box 
          component='img' 
          sx={{height: '50%'}} 
          src='../../assets/icon-pregnant.png' />
      </Fab>
      <Fab onClick={() => navigate('baby')} sx={{backgroundColor: '#82cccf', ...buttonsSize}}>
        <Box 
          component='img' 
          sx={{height: '50%'}} 
          src='../../assets/icon-baby.png' />
      </Fab>
      <Fab onClick={() => navigate('kid')} sx={{backgroundColor: '#75d575', ...buttonsSize}}>
        <Box
          component='img' 
          sx={{height: '50%'}} 
          src='../../assets/icon-kids.png' />
      </Fab>
		</Stack>
	);
};

export default Home;