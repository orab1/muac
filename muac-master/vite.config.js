import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import { VitePWA } from 'vite-plugin-pwa';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react(), 
    VitePWA({ 
      registerType: 'autoUpdate',
      manifest: {
        icons: [
          {
            src: '/assets/icon-baby.png',
            sizes: '328x468',
            type: 'image/png'
          },
          {
            src: '/assets/icon-pregnant.png',
            sizes: '328x468',
            type: 'image/png'
          },
          {
            src: '/assets/icon-measuring_tape.png',
            sizes: '328x468',
            type: 'image/png'
          },
          {
            src: '/assets/icon-kid.png',
            sizes: '328x468',
            type: 'image/png'
          },
        ]
      } 
    })
  ],
})
